'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">ngx-admin documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' : 'data-target="#xs-components-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' :
                                            'id="xs-components-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BomComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BomComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BomDetailDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BomDetailDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FilterDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FilterDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FilterTwoDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FilterTwoDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PersonalisationDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PersonalisationDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StartupWizardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StartupWizardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VendorDetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">VendorDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VerifyDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">VerifyDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' : 'data-target="#xs-injectables-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' :
                                        'id="xs-injectables-links-module-AppModule-610f8d0627c9ddd1fffac898065fa494"' }>
                                        <li class="link">
                                            <a href="injectables/DateUtils.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DateUtils</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JWTTokenUtils.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>JWTTokenUtils</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConfigurationModule.html" data-type="entity-link">ConfigurationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConfigurationModule-cce26277184d5e609c127b9b65e8691c"' : 'data-target="#xs-components-links-module-ConfigurationModule-cce26277184d5e609c127b9b65e8691c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConfigurationModule-cce26277184d5e609c127b9b65e8691c"' :
                                            'id="xs-components-links-module-ConfigurationModule-cce26277184d5e609c127b9b65e8691c"' }>
                                            <li class="link">
                                                <a href="components/ConfigurationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfigurationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProcessComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProcessComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProcessParameterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProcessParameterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProcessTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProcessTypeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfigurationRoutingModule.html" data-type="entity-link">ConfigurationRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardModule.html" data-type="entity-link">DashboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DashboardModule-7fb80d289694551df3a889f84212e5be"' : 'data-target="#xs-components-links-module-DashboardModule-7fb80d289694551df3a889f84212e5be"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardModule-7fb80d289694551df3a889f84212e5be"' :
                                            'id="xs-components-links-module-DashboardModule-7fb80d289694551df3a889f84212e5be"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TolerenceDeviationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TolerenceDeviationComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/IOTDashboardModule.html" data-type="entity-link">IOTDashboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-IOTDashboardModule-32bbc16585fbc9eb3da1bb356c2f6a84"' : 'data-target="#xs-components-links-module-IOTDashboardModule-32bbc16585fbc9eb3da1bb356c2f6a84"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-IOTDashboardModule-32bbc16585fbc9eb3da1bb356c2f6a84"' :
                                            'id="xs-components-links-module-IOTDashboardModule-32bbc16585fbc9eb3da1bb356c2f6a84"' }>
                                            <li class="link">
                                                <a href="components/ContactsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ElectricityChartComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ElectricityChartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ElectricityComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ElectricityComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IOTDashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">IOTDashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/KittenComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">KittenComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PlayerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlayerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomSelectorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoomSelectorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoomsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SecurityCamerasComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SecurityCamerasComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SolarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SolarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StatusCardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StatusCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TemperatureComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TemperatureComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TemperatureDraggerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TemperatureDraggerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TrafficChartComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TrafficChartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TrafficComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TrafficComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WeatherComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">WeatherComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MiscellaneousModule.html" data-type="entity-link">MiscellaneousModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MiscellaneousModule-7e8ab1ecb996fa71e3e47e2f62796006"' : 'data-target="#xs-components-links-module-MiscellaneousModule-7e8ab1ecb996fa71e3e47e2f62796006"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MiscellaneousModule-7e8ab1ecb996fa71e3e47e2f62796006"' :
                                            'id="xs-components-links-module-MiscellaneousModule-7e8ab1ecb996fa71e3e47e2f62796006"' }>
                                            <li class="link">
                                                <a href="components/MiscellaneousComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MiscellaneousComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MiscellaneousRoutingModule.html" data-type="entity-link">MiscellaneousRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModalOverlaysModule.html" data-type="entity-link">ModalOverlaysModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ModalOverlaysModule-b3e01f49cee084537e53eee2f44f4cfc"' : 'data-target="#xs-components-links-module-ModalOverlaysModule-b3e01f49cee084537e53eee2f44f4cfc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalOverlaysModule-b3e01f49cee084537e53eee2f44f4cfc"' :
                                            'id="xs-components-links-module-ModalOverlaysModule-b3e01f49cee084537e53eee2f44f4cfc"' }>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/OneColumnLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OneColumnLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchInputComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchInputComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ThreeColumnsLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ThreeColumnsLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TinyMCEComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TinyMCEComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TwoColumnsLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TwoColumnsLayoutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalOverlaysRoutingModule.html" data-type="entity-link">ModalOverlaysRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NgxAuthModule.html" data-type="entity-link">NgxAuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NgxAuthModule-0116e56e13a69d189e63b01fa0d5bd13"' : 'data-target="#xs-components-links-module-NgxAuthModule-0116e56e13a69d189e63b01fa0d5bd13"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NgxAuthModule-0116e56e13a69d189e63b01fa0d5bd13"' :
                                            'id="xs-components-links-module-NgxAuthModule-0116e56e13a69d189e63b01fa0d5bd13"' }>
                                            <li class="link">
                                                <a href="components/NgxAuthComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxAuthComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxLoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxLoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxResetPasswordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxResetPasswordComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NgxAuthRoutingModule.html" data-type="entity-link">NgxAuthRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/OnboardModule.html" data-type="entity-link">OnboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-OnboardModule-1828f1040cce24d8cbd5d6d8367627ab"' : 'data-target="#xs-components-links-module-OnboardModule-1828f1040cce24d8cbd5d6d8367627ab"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-OnboardModule-1828f1040cce24d8cbd5d6d8367627ab"' :
                                            'id="xs-components-links-module-OnboardModule-1828f1040cce24d8cbd5d6d8367627ab"' }>
                                            <li class="link">
                                                <a href="components/CustomerOnboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CustomerOnboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/OnboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OnboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VendorOnboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">VendorOnboardComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/OnboardRoutingModule.html" data-type="entity-link">OnboardRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PagesModule.html" data-type="entity-link">PagesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PagesModule-54b621dcf7d224c688d43e839d5bec61"' : 'data-target="#xs-components-links-module-PagesModule-54b621dcf7d224c688d43e839d5bec61"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PagesModule-54b621dcf7d224c688d43e839d5bec61"' :
                                            'id="xs-components-links-module-PagesModule-54b621dcf7d224c688d43e839d5bec61"' }>
                                            <li class="link">
                                                <a href="components/ConnectivityComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConnectivityComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GoodsReceiptComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GoodsReceiptComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PagesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PagesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductionModeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProductionModeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QualityDataComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">QualityDataComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PagesRoutingModule.html" data-type="entity-link">PagesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ThemeModule.html" data-type="entity-link">ThemeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' : 'data-target="#xs-components-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' :
                                            'id="xs-components-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' }>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/OneColumnLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OneColumnLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchInputComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchInputComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ThreeColumnsLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ThreeColumnsLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TinyMCEComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TinyMCEComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TwoColumnsLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TwoColumnsLayoutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' : 'data-target="#xs-pipes-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' :
                                            'id="xs-pipes-links-module-ThemeModule-311d8c4a9d2f909f9e0e9f4ca261284d"' }>
                                            <li class="link">
                                                <a href="pipes/CapitalizePipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CapitalizePipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/NumberWithCommasPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NumberWithCommasPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/PluralPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PluralPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/RoundPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoundPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/TimingPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimingPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TracexReportModule.html" data-type="entity-link">TracexReportModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TracexReportModule-024764a3002e9b09aaff62df6fc1419c"' : 'data-target="#xs-components-links-module-TracexReportModule-024764a3002e9b09aaff62df6fc1419c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TracexReportModule-024764a3002e9b09aaff62df6fc1419c"' :
                                            'id="xs-components-links-module-TracexReportModule-024764a3002e9b09aaff62df6fc1419c"' }>
                                            <li class="link">
                                                <a href="components/BomComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BomComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchMaterialComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchMaterialComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TracexReportComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TracexReportComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TracexReportRoutingModule.html" data-type="entity-link">TracexReportRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/BomComponent-1.html" data-type="entity-link">BomComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/DialogComponent.html" data-type="entity-link">DialogComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/DialogNamePromptComponent.html" data-type="entity-link">DialogNamePromptComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/IotDataDialogComponent.html" data-type="entity-link">IotDataDialogComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalOverlaysComponent.html" data-type="entity-link">ModalOverlaysComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/QualityDataDialogComponent.html" data-type="entity-link">QualityDataDialogComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ShowcaseDialogComponent.html" data-type="entity-link">ShowcaseDialogComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Characteristic.html" data-type="entity-link">Characteristic</a>
                            </li>
                            <li class="link">
                                <a href="classes/Company.html" data-type="entity-link">Company</a>
                            </li>
                            <li class="link">
                                <a href="classes/Group.html" data-type="entity-link">Group</a>
                            </li>
                            <li class="link">
                                <a href="classes/IotData.html" data-type="entity-link">IotData</a>
                            </li>
                            <li class="link">
                                <a href="classes/Lookup.html" data-type="entity-link">Lookup</a>
                            </li>
                            <li class="link">
                                <a href="classes/NbSimpleRoleProvider.html" data-type="entity-link">NbSimpleRoleProvider</a>
                            </li>
                            <li class="link">
                                <a href="classes/Node.html" data-type="entity-link">Node</a>
                            </li>
                            <li class="link">
                                <a href="classes/Process.html" data-type="entity-link">Process</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProcessParameter.html" data-type="entity-link">ProcessParameter</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProcessType.html" data-type="entity-link">ProcessType</a>
                            </li>
                            <li class="link">
                                <a href="classes/QDLineItem.html" data-type="entity-link">QDLineItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/QualityData.html" data-type="entity-link">QualityData</a>
                            </li>
                            <li class="link">
                                <a href="classes/QualityPostData.html" data-type="entity-link">QualityPostData</a>
                            </li>
                            <li class="link">
                                <a href="classes/Role.html" data-type="entity-link">Role</a>
                            </li>
                            <li class="link">
                                <a href="classes/Service.html" data-type="entity-link">Service</a>
                            </li>
                            <li class="link">
                                <a href="classes/StartupWizard.html" data-type="entity-link">StartupWizard</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/BomService.html" data-type="entity-link">BomService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BomService-1.html" data-type="entity-link">BomService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConnectivityService.html" data-type="entity-link">ConnectivityService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/Constants.html" data-type="entity-link">Constants</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CustomerOnboardService.html" data-type="entity-link">CustomerOnboardService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DalService.html" data-type="entity-link">DalService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DateUtils.html" data-type="entity-link">DateUtils</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GoodsReceiptService.html" data-type="entity-link">GoodsReceiptService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JWTTokenUtils.html" data-type="entity-link">JWTTokenUtils</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MasterDataService.html" data-type="entity-link">MasterDataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/QualityDataService.html" data-type="entity-link">QualityDataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SharedService.html" data-type="entity-link">SharedService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TolerenceDeviationService.html" data-type="entity-link">TolerenceDeviationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TracexReportService.html" data-type="entity-link">TracexReportService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/CardSettings.html" data-type="entity-link">CardSettings</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CharacteristicsArray.html" data-type="entity-link">CharacteristicsArray</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IMainObject.html" data-type="entity-link">IMainObject</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ItemsArray.html" data-type="entity-link">ItemsArray</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SearchObjectArray.html" data-type="entity-link">SearchObjectArray</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SelectedFiltersArray.html" data-type="entity-link">SelectedFiltersArray</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/selectList.html" data-type="entity-link">selectList</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/selectList-1.html" data-type="entity-link">selectList</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});